CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Benefits
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Backup and Migrate Yandex Disk module provides support functions for module "Backup and Migrate" to
automatically backups for Yandex Disk.


BENEFITS
--------

Free cloud storage for backups.


REQUIREMENTS
------------

This module requires the following module:

 * Backup and Migrate - https://www.drupal.org/project/backup_migrate


INSTALLATION
------------

Install the module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
further information. Note that there are two dependencies.


CONFIGURATION
-------------

   1. Navigate to Administration > Extend and enable the "Backup and Migrate on Yandex Disk" module.
   2. If not have account on yandex.ru, create it! 
   3. Configure the module at admin/config/development/backup_migrate/yandex-disk-settings - clicking "Create new app" 
   and register the application as described in the documentation https://yandex.ru/dev/oauth/doc/dg/tasks/register-client-docpage/. 
   Required fields: "App name", "Platform", "Callback URI (https://example.com/yandex-disk-verification-callback)", "REST API (all items)" and save app.
   4. Fill "ID" and "Password" on admin/config/development/backup_migrate/yandex-disk-settings form and click "Save configuration".
   5. After save settings, "Get token" button will be available. Click "Get token" and approve and confirm rights on redirected page.
   6. If everything went without errors, you will be redirected back to the form and you see "Token expires" and "Yandex disk information"
   7. Go to "Destinations" admin/config/development/backup_migrate/settings/destination and create destination.
   8. Use it.


MAINTAINERS
-----------

Current maintainers:

 * Pavel Popov - https://www.drupal.org/u/pazitiff
