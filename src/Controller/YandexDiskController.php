<?php

namespace Drupal\backup_migrate_yandex_disk\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Component\Serialization\Json;

class YandexDiskController extends ControllerBase {

  protected $configFactory;

  protected $oauthHttpClient;

  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $oauth_client) {
    $this->configFactory = $config_factory;
    $this->oauthHttpClient = $oauth_client;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('backup_migrate_yandex_disk_oauth.client')
    );
  }

  public function token() {
    $code = \Drupal::request()->query->get('code');
    if (!empty($code)) {
      // Идентификатор приложения
      $configName = 'backup_migrate_yandex_disk.settings';
      $config = $this->configFactory->get($configName);
      $body = http_build_query([
        'grant_type' => 'authorization_code',
        'code' => $code,
        'client_id' => $config->get('id'),
        'client_secret' => $config->get('password'),
      ]);
      $response = $this->oauthHttpClient->post('/token', [
        'body' => $body,
      ]);

      $responseBody = Json::decode($response->getBody());
      // Токен необходимо сохранить для использования в запросах к API Директа
      if (is_array($responseBody)) {
        $this->configFactory->getEditable($configName)
          ->set('token', $responseBody['access_token'])
          ->set('token_expires', $responseBody['expires_in'])
          ->set('token_type', $responseBody['token_type'])
          ->set('refresh_token', $responseBody['refresh_token'])
          ->set('token_end_time', time() + $responseBody['expires_in'])
          ->save();
      }
    }
    $route = Url::fromRoute('backup_migrate_yandex_disk.settings')->toString();
    return new RedirectResponse($route);
  }
}
