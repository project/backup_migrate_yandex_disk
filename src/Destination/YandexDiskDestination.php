<?php

namespace Drupal\backup_migrate_yandex_disk\Destination;

use BackupMigrate\Core\Config\ConfigurableInterface;
use BackupMigrate\Core\Plugin\FileProcessorInterface;
use BackupMigrate\Core\File\BackupFile;
use BackupMigrate\Core\File\BackupFileInterface;
use BackupMigrate\Core\File\BackupFileReadableInterface;
use BackupMigrate\Core\Destination\DestinationBase;
use BackupMigrate\Core\Destination\ListableDestinationInterface;
use BackupMigrate\Core\Destination\ReadableDestinationInterface;
use BackupMigrate\Core\Destination\SidecarMetadataDestinationTrait;
use Drupal\backup_migrate_yandex_disk\File\ReadableStreamBackupFileYandexDisk;
use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\RequestException;

/**
 * Class YandexDiskDestination.
 */
class YandexDiskDestination extends DestinationBase implements ListableDestinationInterface, ReadableDestinationInterface, ConfigurableInterface, FileProcessorInterface {

  use SidecarMetadataDestinationTrait;

  protected $httpClient;

  protected $configFactory;

  protected $messenger;

  public function __construct($init = []) {
    parent::__construct($init);
    $this->configFactory = \Drupal::service('config.factory');
    $this->httpClient = \Drupal::service('backup_migrate_yandex_disk.client');
    $this->messenger = \Drupal::messenger();
  }

  protected function getLogger() {
    return \Drupal::logger('backup_migrate_yandex_disk');
  }

  /**
   * {@inheritdoc}
   */
  function saveFile(BackupFileReadableInterface $file) {
    $this->_saveFile($file);
    $this->_saveFileMetadata($file);
  }

  /**
   * {@inheritdoc}
   */
  public function checkWritable() {
    $this->checkDirectory();
  }

  /**
   * Get a definition for user-configurable settings.
   *
   * @param array $params
   *
   * @return array
   */
  public function configSchema($params = []) {
    $schema = [];
    // Init settings.
    if ($params['operation'] == 'initialize') {
      $schema['fields']['directory'] = [
        'type' => 'text',
        'title' => $this->t('Yandex Disk Directory Path'),
      ];
    }
    return $schema;
  }

  public function createDir($directory) {
    $dirName = '/' . ltrim($directory, '/');
    if ($this->fileExists($dirName) === FALSE) {
      try {
        $this->httpClient->put('/v1/disk/resources', ['query' => ['path' => $dirName]]);
      } catch (RequestException $e) {
        $this->getLogger()->error($e->getMessage());
        if($e->getCode() == 409) {
          return TRUE;
        }
//        $this->messenger->addMessage($e->getMessage(), 'error');
        return FALSE;
      }
    }
    return TRUE;
  }

  public function deleteDir($directory) {
    try {
      $response = $this->httpClient->delete('/v1/disk/resources', ['query' => ['path' => $directory]]);
    } catch (RequestException $e) {
      $this->getLogger()->error($e->getMessage());
      $this->messenger->addMessage($e->getMessage(), 'error');
    }
    if ($response->getStatusCode() == 204) {
      return TRUE;
    }
    return FALSE;
  }

  function _saveFile(BackupFileReadableInterface $file) {
    $directory = $this->confGet('directory');
    $filepath = $directory . '/' . $file->getName() . '.' . $file->getExt();
    try {
      $response = $this->httpClient->get('/v1/disk/resources/upload', ['query' => ['path' => $filepath]]);
    } catch (RequestException $e) {
      $this->getLogger()->error($e->getMessage());
      $this->messenger->addMessage($e->getMessage(), 'error');
    }

    $responseBody = Json::decode($response->getBody());
    if (empty($responseBody['error'])) {
      $resource = fopen($file->realpath(), 'r');
      try {
        $httpCode = $this->httpClient->put($responseBody['href'], ['body' => $resource])->getStatusCode();
      } catch (RequestException $e) {
        $this->getLogger()->error($e->getMessage());
//        $this->messenger->addMessage($e->getMessage(), 'error');
      }
      //      @TODO - https://yandex.ru/dev/disk/poligon/#!//v1/disk/resources
      if ($httpCode == 201) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Check that the directory can be used for backup.
   *
   * @throws \BackupMigrate\Core\Exception\BackupMigrateException
   */
  protected function checkDirectory() {
    $dir = $this->confGet('directory');
    $path = $this->confGet('directory');
    $fields = '_embedded.items.name,_embedded.items.type';
  }

  /**
   * {@inheritdoc}
   */
  public function getFile($id) {
    if ($this->fileExists($id)) {
      $out = new BackupFile();
      $out->setMeta('id', $id);
      $out->setFullName($id);
      return $out;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadFileForReading(BackupFileInterface $file) {
    if ($file instanceof BackupFileReadableInterface) {
      return $file;
    }

    $id = $file->getMeta('id');
    $file = $this->fileExists($id);
    if (!empty($file)) {
      return new ReadableStreamBackupFileYandexDisk($file);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function listFiles($count = 100, $start = 0) {
    $path = $this->confGet('directory');
    try {
      $response = $this->httpClient->get('/v1/disk/resources', [
        'query' => [
          'path' => $path,
          'limit' => $count,
          'offset' => $start,
        ],
      ]);
    } catch (RequestException $e) {
      $this->getLogger()->error($e->getMessage());
      $this->messenger->addMessage($e->getMessage(), 'error');
    }
    $responseBody = Json::decode($response->getBody());
    $responseFiles = $responseBody['_embedded']['items'];
    $files = [];
    foreach ((array) $responseFiles as $file) {
      if (substr($file['name'], strlen($file) - 5) !== '.info') {
        $out = new BackupFile();
        $out->setMeta('id', $file['name']);
        $out->setMeta('filesize', $file['size']);
        $out->setMeta('datestamp', date("U", strtotime($file['created'])));
        //        $out->setMetaMultiple($file);
        $out->setFullName($file['name']);
        $files[$file['name']] = $out;
      }
    }
    return $files;
  }

  /**
   * {@inheritdoc} список файлов
   */
  public function queryFiles($filters = [], $sort = NULL, $sortDirection = SORT_DESC, $count = 100, $start = 0) {
    $count = empty($count) ? 100 : $count;
    $out = $this->listFiles($count, $start);
    foreach ($out as $key => $file) {
      $out[$key] = $this->loadFileMetadata($file);
    }

    // Filter the output.
    if ($filters) {
      $out = array_filter($out, function ($file) use ($filters) {
        foreach ($filters as $key => $value) {
          if ($file->getMeta($key) !== $value) {
            return FALSE;
          }
        }
        return TRUE;
      });
    }

    // Sort the files.
    if ($sort && $sortDirection) {
      uasort($out, function ($a, $b) use ($sort, $sortDirection) {
        if ($sortDirection == SORT_DESC) {
          return $b->getMeta($sort) < $b->getMeta($sort);
        }
        else {
          return $b->getMeta($sort) > $b->getMeta($sort);
        }
      });
    }

    // Slice the return array.
    if ($count || $start) {
      $out = array_slice($out, $start, $count);
    }
    return $out;
  }

  /**
   * @return int The number of files in the destination.
   */
  public function countFiles() {
    $files = $this->_getAllFileNames();
    return count($files);
  }

  public function fileExists($id) {
    $filepath = $this->_idToPath($id);
    try {
      $response = $this->httpClient->get('/v1/disk/resources', ['query' => ['path' => $filepath]]);
    } catch (RequestException $e) {
      $this->getLogger()->error($e->getMessage());
      if ($e->getCode() == 404) {
        return FALSE;
      }
    }

    $responseBody = Json::decode($response->getBody());
    $responseBody['filepath'] = $filepath;
    return $responseBody;
  }

  /**
   * {@inheritdoc}
   */
  public function _deleteFile($id) {
    if ($file = $this->getFile($id)) {
      if ($file = $this->loadFileForReading($file)) {
        $filepath = $this->_idToPath($id);
        try {
          $response = $this->httpClient->delete('/v1/disk/resources', ['query' => ['path' => $filepath]]);
        } catch (RequestException $e) {
          $this->getLogger()->error($e->getMessage());
          $this->messenger->addMessage($e->getMessage(), 'error');
        }
        if ($response->getStatusCode() == 204) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Return a file path for the given file id.
   *
   * @param $id
   *
   * @return string
   */
  protected function _idToPath($id) {
    return rtrim($this->confGet('directory'), '/') . '/' . $id;
  }

  /**
   * Get the entire file list from this destination.
   *
   * @return array
   */
  protected function _getAllFileNames() {
    $files = [];

    // Read the list of files from the directory.
    $dir = $this->confGet('directory');

    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = \Drupal::service('file_system');
    $scheme = $fileSystem->uriScheme($dir);

    // Ensure the stream is configured.
    if (!$fileSystem->validScheme($scheme)) {
      $this->messenger->addMessage(t('Your @scheme stream is not configured.', [
        '@scheme' => $scheme . '://',
      ]), 'warning');
      return $files;
    }

    if ($handle = opendir($dir)) {
      while (FALSE !== ($file = readdir($handle))) {
        $filepath = $dir . '/' . $file;
        // Don't show hidden, unreadable or metadata files.
        if (substr($file, 0, 1) !== '.' && is_readable($filepath) && substr($file, strlen($file) - 5) !== '.info') {
          $files[] = $file;
        }
      }
    }
    return $files;
  }

  /**
   * {@inheritdoc}
   */
  protected function _loadFileMetadataArray(BackupFileInterface $file) {
    $info = [];
    $id = $file->getFullName() . '.info';
    if ($this->fileExists($id)) {
      $metaFile = $this->getFile($id);
      $metaFile = $this->loadFileForReading($metaFile);
      $info = $this->_INIToArray($metaFile->readAll());
    }
    return $info;
  }
}
