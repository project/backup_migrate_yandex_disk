<?php

/**
 * @file
 */
namespace Drupal\backup_migrate_yandex_disk\Exception;
use BackupMigrate\Core\Exception\BackupMigrateException;
/**
 * Class YandexDiskException.
 *
 */
class YandexDiskException extends BackupMigrateException {}
