<?php
/**
 * @file
 * Contains BackupMigrate\Core\File\ReadableStream.
 */

namespace Drupal\backup_migrate_yandex_disk\File;

use BackupMigrate\Core\File\BackupFileReadableInterface;
use BackupMigrate\Core\File\BackupFile;

/**
 * Class ReadableStreamBackupFileYandexDisk.
 *
 * An implementation of the BackupFileReadableInterface which uses a readable
 * php stream such as a local file.
 */
class ReadableStreamBackupFileYandexDisk extends BackupFile implements BackupFileReadableInterface {

  protected $handle;

  protected $cloudFile;

  function __construct($file) {
    $this->cloudFile = $file;
    $filepath = $file['file'];
    $this->handle = fopen($filepath, 'r');
    $this->path = $filepath;
    $this->setFullName(basename($file['name']));
    $this->_loadFileStats();
  }

  function __destruct() {
    $this->close();
  }

  function realpath() {
    return $this->path;
  }

  function openForRead($binary = FALSE) {
    if (!$this->isOpen()) {
      $path = $this->cloudFile['file'];
      $mode = "r" . ($binary ? "b" : "");
      $this->handle = fopen($path, $mode);
      if (!$this->handle) {
        throw new \Exception('Cannot open file.');
      }
    }
    // If the file is already open, rewind it.
    //    $this->rewind();
    return $this->handle;
  }

  function close() {
    if ($this->isOpen()) {
      fclose($this->handle);
      $this->handle = NULL;
    }
  }

  function isOpen() {
    return !empty($this->handle) && get_resource_type($this->handle) == 'stream';
  }

  function readBytes($size = 1024, $binary = FALSE) {
    if (!$this->isOpen()) {
      $this->openForRead($binary);
    }
    if ($this->handle && !feof($this->handle)) {
      return fread($this->handle, $size);
    }
    return NULL;
  }

  public function readLine() {
    if (!$this->isOpen()) {
      $this->openForRead();
    }
    return fgets($this->handle);
  }

  public function readAll() {
    if (!$this->isOpen()) {
      $this->openForRead();
    }
//    $this->rewind();
    return stream_get_contents($this->handle);
  }

  public function seekBytes($bytes) {
    if ($this->isOpen()) {
      return fseek($this->handle, $bytes);
    }
    return -1;
  }

  function rewind() {
    if ($this->isOpen()) {
      rewind($this->handle);
    }
  }

  protected function _loadFileStats() {
    clearstatcache();
    $this->setMeta('filesize', $this->cloudFile['size']);
    $this->setMeta('datestamp', date("U", strtotime($this->cloudFile['created'])));
  }

}
