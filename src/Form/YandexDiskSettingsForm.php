<?php


namespace Drupal\backup_migrate_yandex_disk\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger\MessengerInterface;

class YandexDiskSettingsForm extends ConfigFormBase {

  protected $oauthHttpClient;

  protected $httpClient;

  protected $ydSettings;

  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, ClientInterface $client, ClientInterface $oauth_client) {
    parent::__construct($config_factory);
    $this->httpClient = $client;
    $this->oauthHttpClient = $oauth_client;
    $this->ydSettings = $this->config('backup_migrate_yandex_disk.settings');
    $this->setMessenger($messenger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('backup_migrate_yandex_disk.client'),
      $container->get('backup_migrate_yandex_disk_oauth.client')
    );
  }

  public function getEditableConfigNames() {
    return [
      'backup_migrate_yandex_disk.settings',
    ];
  }

  public function getFormId() {
    return 'yandex_disk_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $oauthUrl = $this->oauthHttpClient->getConfig('base_uri')->__toString();
    $form['link_to_app'] = [
      '#title' => $this->t('Create new app'),
      '#type' => 'link',
      '#url' => Url::fromUri($oauthUrl . '/client/new', [
        'attributes' => [
          'class' => [
            'button',
          ],
          'target' => '_blank',
        ],
      ]),
    ];

    $form['yandex_disk_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Setting'),
    ];

    $form['yandex_disk_settings']['id'] = [
      '#title' => $this->t('ID'),
      '#type' => 'textfield',
      '#description' => $this->t('Application ID'),
      '#default_value' => $this->ydSettings->get('id'),
      '#required' => TRUE,
    ];

    $form['yandex_disk_settings']['password'] = [
      '#title' => $this->t('Password'),
      '#type' => 'textfield',
      '#description' => $this->t('Application password'),
      '#default_value' => $this->ydSettings->get('password'),
      '#required' => TRUE,
    ];

    $callback_url = \Drupal::request()->getSchemeAndHttpHost() . '/yandex-disk-verification-callback';

    $form['yandex_disk_settings']['callback'] = [
      '#title' => $this->t('Callback URI'),
      '#type' => 'item',
      '#markup' => $callback_url,
    ];

    $form['yandex_disk_settings']['token_info'] = [
      '#type' => 'container',
    ];

    $form['yandex_disk_settings']['token_info']['token'] = [
      '#title' => $this->t('Token information'),
      '#type' => 'item',
      '#markup' => empty($this->ydSettings->get('token')) ? 'Token not found' : 'Token exists',
    ];

    $date = $this->t('not found');
    if (!empty($this->ydSettings->get('token_end_time'))) {
      $seconds = $this->ydSettings->get('token_end_time') - time();
      $dtF = new \DateTime('@0');
      $dtT = new \DateTime("@$seconds");
      $date = $dtF->diff($dtT)->format('%a days, %h hours, %i minutes');
    }

    $form['yandex_disk_settings']['token_info']['expires'] = [
      '#title' => $this->t('Token expires in'),
      '#type' => 'item',
      '#markup' => $date,
    ];

    $form['yandex_disk_settings']['actions'] = [
      '#type' => 'actions',
    ];

    if (!empty($this->ydSettings->get('id')) && !empty($this->ydSettings->get('password'))) {
      if (empty($this->ydSettings->get('token'))) {
        $form['yandex_disk_settings']['actions']['link_to_get_token'] = [
          '#title' => $this->t('Get token'),
          '#type' => 'link',
          '#url' => Url::fromUri($oauthUrl . '/authorize', [
            'query' => [
              'client_id' => $this->ydSettings->get('id'),
              'redirect_uri' => $callback_url,
              'response_type' => 'code',
              'scope' => implode(' ', [
                "cloud_api:disk.app_folder",
                "cloud_api:disk.read",
                "cloud_api:disk.write",
                "cloud_api:disk.info",
              ]),
            ],
            'attributes' => [
              'class' => [
                'button',
              ],
              'target' => '_blank',
            ],
          ]),
        ];
      }
      else {
        $form['yandex_disk_settings']['actions']['refresh_token'] = [
          '#type' => 'submit',
          '#value' => 'refresh token',
          '#submit' => ['::refreshToken'],
        ];
      }
    }

    $form['yandex_disk_dev_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Dev setting'),
    ];

    $form['yandex_disk_dev_settings']['develop_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Dev mode'),
      '#default_value' => $this->ydSettings->get('develop_mode'),
    ];

    $form['yandex_disk_dev_settings']['settings'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'develop-mode',
      ],
      '#states' => [
        'invisible' => [
          'input[name="develop_mode"]' => [
            'checked' => FALSE,
          ],
        ],
      ],
    ];

    $form['yandex_disk_dev_settings']['settings']['develop_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dev token'),
      '#default_value' => $this->ydSettings->get('develop_token'),
    ];

    $form['yandex_disk_info'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Yandex disk information'),
    ];

    $fields = [
      'total_space' => 'Total space',
      'used_space' => 'Used space',
      'max_file_size' => 'Max file size',
    ];

    foreach ($fields as $key => $item) {
      $title = $fields[$key];
      $form['yandex_disk_info'][$key] = [
        '#type' => 'item',
        '#title' => $this->t($title),
        '#markup' => 0,
      ];
    }

    if (!empty($this->ydSettings->get('token')) || ($this->ydSettings->get('develop_mode') && !empty($this->ydSettings->get('develop_token')))) {
      $fieldsStr = implode(',', array_keys($fields));
      try {
        $response = $this->httpClient->get('/v1/disk', ['query' => ['fields' => $fieldsStr]]);
      } catch (\Exception $e) {
        if ($e->getCode() == 404) {
          return FALSE;
        }
      }
      $result = Json::decode($response->getBody());
      foreach ($result as $key => $item) {
        $form['yandex_disk_info'][$key]['#markup'] = format_size($item);
      }
    }

    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  public function refreshToken(array &$form, FormStateInterface $form_state) {
    $query = [
      'grant_type' => 'refresh_token',
      'refresh_token' => $this->ydSettings->get('refresh_token'),
      'client_id' => $this->ydSettings->get('id'),
      'client_secret' => $this->ydSettings->get('password'),
    ];

    $response = $this->oauthHttpClient->post('/token', [
      'headers' => [
        'Authorization' => 'Basic ' . base64_encode($this->ydSettings->get('id') . ':' . $this->ydSettings->get('password')),
      ],
      'body' => http_build_query($query),
    ]);

    $responseBody = Json::decode($response->getBody());
    if ($responseBody) {
      $end_token = time() + $responseBody['expires_in'];
      \Drupal::configFactory()
        ->getEditable('backup_migrate_yandex_disk.settings')
        ->set('token', $responseBody['access_token'])
        ->set('refresh_token', $responseBody['refresh_token'])
        ->set('token_type', $responseBody['token_type'])
        ->set('token_expires', $responseBody['expires_in'])
        ->set('token_end_time', $end_token)
        ->save();
      $this->messenger()->addMessage($this->t('The token has been updated.'));
    }
    else {
      $this->messenger()->addWarning($this->t('The token has not been updated.'));
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::cache('discovery')->delete('backup_migrate_destination_plugins');
    \Drupal::configFactory()
      ->getEditable('backup_migrate_yandex_disk.settings')
      ->set('id', $form_state->getValue('id'))
      ->set('password', $form_state->getValue('password'))
      ->set('token', $form_state->getValue('token'))
      ->set('develop_mode', $form_state->getValue('develop_mode'))
      ->set('develop_token', $form_state->getValue('develop_token'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
