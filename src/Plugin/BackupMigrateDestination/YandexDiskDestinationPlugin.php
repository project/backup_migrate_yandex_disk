<?php

namespace Drupal\backup_migrate_yandex_disk\Plugin\BackupMigrateDestination;

use BackupMigrate\Drupal\EntityPlugins\DestinationPluginBase;
/**
 * Defines a file directory destination plugin.
 *
 * @BackupMigrateDestinationPlugin(
 *   id = "YandexDisk",
 *   title = @Translation("Yandex Disk"),
 *   description = @Translation("Back up to a directory on yandex disk."),
 *   wrapped_class = "\Drupal\backup_migrate_yandex_disk\Destination\YandexDiskDestination"
 * )
 */
class YandexDiskDestinationPlugin extends DestinationPluginBase {}
