<?php


namespace Drupal\backup_migrate_yandex_disk;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;

class YandexDiskClientFactory {

  protected $clientConfig;

  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->clientConfig = $config_factory->get('backup_migrate_yandex_disk.settings');
  }

  /**
   * Return a configured Client object.
   */
  public function get() {
    $options = [];
    $devMode = $this->clientConfig->get('develop_mode');
    $token = empty($devMode) ? $this->clientConfig->get('token') : $this->clientConfig->get('develop_token');
    if ($token) {
      $options = [
        'headers' => [
          'Authorization' => 'OAuth ' . $token,
        ],
        'base_uri' => 'https://cloud-api.yandex.net',
      ];
    }
    $client = new Client($options);
    return $client;
  }
}
