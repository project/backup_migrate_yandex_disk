<?php


namespace Drupal\backup_migrate_yandex_disk;

use GuzzleHttp\Client;

class YandexOauthClientFactory {

  /**
   * Return a configured Client object.
   */
  public function get() {
    $options = [
      'headers' => [
        'Content-Type' => 'application/x-www-form-urlencoded',
      ],
      'base_uri' => 'https://oauth.yandex.ru',
    ];

    $client = new Client($options);
    return $client;
  }
}
